#include <iostream>
#include"mymatrix.h"
#include<stdlib.h>
#include<time.h>
using namespace std;

int main() {
    srand(time(NULL));
    int M = 4;
    int N= 4;
    myMatrix test1(M,N,"random");
    myMatrix test2(M,N,"zero");
    test2 = maxpooling(test1);
    cout<<test1;
    cout<<test2;
    return 0;
}
