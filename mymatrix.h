

#ifndef MYMATRIX_H_
#define MYMATRIX_H_
#include<iostream>
using namespace std;
class myMatrix
{
    int **matrix;
    int rows;
    int coloumns;
public:
    myMatrix(int M, int N,string value);
    myMatrix operator+ (const myMatrix&);
    myMatrix operator- (const myMatrix&);
    myMatrix operator* (const myMatrix&);
    myMatrix operator= (const myMatrix&);
    friend myMatrix maxpooling(const myMatrix& rhs);
    friend std::ostream& operator<< (std::ostream&,const myMatrix&);
};




#endif /* MYMATRIX_H_ */
